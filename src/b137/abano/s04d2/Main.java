package b137.abano.s04d2;

import b137.abano.s04d2.abstraction.AnotherPerson;
import b137.abano.s04d2.abstraction.Person;
import b137.abano.s04d2.inheritance.Animal;
import b137.abano.s04d2.inheritance.Dog;
import b137.abano.s04d2.polymorphism.ChildClass1;
import b137.abano.s04d2.polymorphism.ChildClass2;
import b137.abano.s04d2.polymorphism.DynamicPolymorphism;
import b137.abano.s04d2.polymorphism.StaticPolymorphism;

public class Main {
    public static void main(String[] args) {
        System.out.println("Inheritance, Abstraction, & Polymorphism\n");

        Animal firstAnimal = new Animal("Tiger", "Brown");

        firstAnimal.showDetails();

        //
        Dog firstDog = new Dog("Naicha", "Cream", "Aspin");

        firstDog.bark();
        System.out.println(firstDog.getBreed());
        System.out.println(firstDog.getName());
        System.out.println(firstDog.getColor());

        //overwrite a particular method
            //copy the whole method from the parent class
        firstDog.showDetails();

        System.out.println();

        //Abstraction and Interfaces
        Person firstPerson = new Person();  // Create a Pig object
        //invoke Actions
        firstPerson.sleep();
        firstPerson.run();
        //invoke Special Skills
        firstPerson.computerProgramming();
        firstPerson.carDriving();

        System.out.println();

        //Another Person
        AnotherPerson secondPerson = new AnotherPerson();
        //invoke Actions
        secondPerson.sleep();
        secondPerson.run();
        //invoke Special Skills
        secondPerson.computerProgramming();
        secondPerson.carDriving();

        System.out.println();

        StaticPolymorphism poly1 = new StaticPolymorphism();
        System.out.println(poly1.add(1,2));
        System.out.println(poly1.add(1,2, 3));
        System.out.println(poly1.add(1.5,2.4));

        System.out.println();

        DynamicPolymorphism poly2;
        //parent
        poly2 = new DynamicPolymorphism();
        poly2.message();
        //child1
        poly2 = new ChildClass1();
        poly2.message();
        //child2
        poly2 = new ChildClass2();
        poly2.message();
    }
}
