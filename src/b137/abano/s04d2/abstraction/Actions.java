package b137.abano.s04d2.abstraction;

public interface Actions {
    default void sleep(){
        System.out.println("person is sleeping");
    }
    default void run(){
        System.out.println("person is running");
    }
}
