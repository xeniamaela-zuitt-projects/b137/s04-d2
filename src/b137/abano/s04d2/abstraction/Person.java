package b137.abano.s04d2.abstraction;

public class Person implements Actions, SpecialSkills{

    //Empty Constructor
    public Person(){}

    //Method Person Actions
    public void sleep() {
        Actions.super.sleep();
    }
    public void run () {
        Actions.super.run();
    }

    //Method Person Special Skills
    public void computerProgramming() {
        //provide the interface add super and invoke the method
        SpecialSkills.super.computerProgramming();
    }
    public void carDriving() {
        SpecialSkills.super.carDriving();
    }
}
