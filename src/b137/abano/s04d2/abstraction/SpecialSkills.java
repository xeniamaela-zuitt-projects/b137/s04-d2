package b137.abano.s04d2.abstraction;

public interface SpecialSkills {

    default void computerProgramming(){
        //implementation
        System.out.println("person knows computer programming");
    }

    default void carDriving(){
        //implementation
        System.out.println("person knows car driving");
    }
}

