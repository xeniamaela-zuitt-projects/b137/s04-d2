package b137.abano.s04d2.inheritance;

public class Dog extends Animal{
    //Properties
    private String breed;

    //Constructor
    public Dog() {
        super(); ///a function
    }

    public Dog(String name, String color, String breed) {
        super(name,color);
        this.breed = breed;
    }

    //Getter
    public String getBreed() {
        return breed;
    }

    //Setter
    public void setBreed(String newBreed){
        this.breed = newBreed;
    }

    //Method
    public void bark() { super.showDetails(); }

    //overwrite a particular method
        //copy the whole method from the parent class

    //Method
    public void showDetails() {
        //example of polymorphism
        System.out.println(super.getName() + " is color " + super.getColor() + ", breed of " + this.breed);
    }
}
